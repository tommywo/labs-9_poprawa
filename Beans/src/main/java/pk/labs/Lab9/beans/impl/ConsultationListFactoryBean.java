/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;
/**
 *
 * @author Tomek
 */
public class ConsultationListFactoryBean implements ConsultationListFactory {

    public ConsultationList create() {
        return new ConsultationListBean();
    }

    public ConsultationList create(boolean deserialize) {
        ConsultationList List = new ConsultationListBean();
        if (deserialize) {
            try {
                XMLDecoder decoder = new XMLDecoder(
                        new BufferedInputStream(
                                new FileInputStream("pliczek.xml")));
                List.addConsultation((Consultation) decoder.readObject());
                decoder.close();
            } catch (FileNotFoundException | PropertyVetoException ex) {
                Logger.getLogger(ConsultationListFactoryBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            return List;
        } else {
            return create();
        }
    }

    public void save(ConsultationList consultationList) {
        try {
            try (XMLEncoder encoder = new XMLEncoder(
                    new BufferedOutputStream(
                            new FileOutputStream("pliczek.xml")))) {
                        encoder.writeObject(consultationList.getConsultation()[0]);
                        encoder.close();
                    }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConsultationListFactoryBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}