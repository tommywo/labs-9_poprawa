/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author Tomek
 */
public class ConsultationBean implements Serializable, Consultation {

    private String student;
    private Term term;

    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public ConsultationBean() {

    }

    public Term getTerm() {
        return this.term;
    }

    public void setTerm(Term term) throws PropertyVetoException {
        Term oldTerm = this.term;
        vcs.fireVetoableChange("Term", oldTerm, term);
        this.term = term;
        pcs.firePropertyChange("Term", oldTerm, term);
    }

    public String getStudent() {
        return this.student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Date getBeginDate() {
        return this.term.getBegin();
    }

    public Date getEndDate() {
        return this.term.getEnd();
    }

    public void prolong(int minutes) throws PropertyVetoException {
        if (minutes <= 0) {
            minutes = 0;
        }

        Term newTerm = new TermBean();
        newTerm.setBegin(this.term.getBegin());
        newTerm.setDuration(this.term.getDuration() + minutes);
        int oldDuration = this.term.getDuration();
        this.vcs.fireVetoableChange("Term", term, newTerm);
        this.term.setDuration(this.term.getDuration() + minutes);
        this.pcs.firePropertyChange("Term", oldDuration, oldDuration + minutes);
    }

    public void addVeto(Listener listener) {
        this.vcs.addVetoableChangeListener(listener);
    }
}
