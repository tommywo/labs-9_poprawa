/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author Tomek
 */
public class TermBean implements Term, Serializable {

    Date begin;
    int duration;
    Date end;

    public Date getBegin() {
        return this.begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        if (duration > 0) {
            this.duration = duration;
        }

    }

    public Date getEnd() {
        return this.end = new Date(begin.getTime() + this.duration * 60L * 1000);
    }
}
