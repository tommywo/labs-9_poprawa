/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.Arrays;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
/**
 *
 * @author Tomek
 */
public class ConsultationListBean implements ConsultationList, Serializable {

    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private Listener listener = new Listener(this);
    private Consultation[] lista;

    public ConsultationListBean() {
        this.lista = new Consultation[]{};
    }

    public int getSize() {
        return this.lista.length;
    }

    public Consultation[] getConsultation() {
        return this.lista;
    }
    
    public Consultation getConsultation(int index) {
        return this.lista[index];
    }

    public void addConsultation(Consultation consult) throws PropertyVetoException {
        for (int i = 0; i < this.lista.length; i++) {
            if ((consult.getEndDate().after(this.lista[i].getBeginDate()) && consult.getBeginDate().before(this.lista[i].getBeginDate()))
                    || (consult.getBeginDate().before(this.lista[i].getEndDate()) && consult.getEndDate().after(this.lista[i].getBeginDate()))) {
                throw new PropertyVetoException("ConsultationListBean PropertyVetoException", null);
            }
        }

        Consultation[] stara = this.lista;
        Consultation[] nowa = Arrays.copyOf(this.lista, this.lista.length + 1);
        nowa[nowa.length - 1] = consult;
        ((ConsultationBean) consult).addVeto(listener);
        this.lista = nowa;
        pcs.firePropertyChange("consultation", stara, nowa);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }
}
