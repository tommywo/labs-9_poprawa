/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
/**
 *
 * @author Tomek
 */
public class Listener implements VetoableChangeListener { 
    private ConsultationListBean lista; 

    public Listener(ConsultationListBean lista) {
        this.lista = lista;
    } 

    @Override public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException { 
        if (event.getPropertyName().equals("Term")) { 
            TermBean stary = (TermBean)event.getOldValue();
            TermBean nowy = (TermBean)event.getNewValue();
        
        for(int i=0; i < this.lista.getSize() - 1; i++) { 
            if(this.lista.getConsultation(i) == stary) {
                continue;
            }   
            if(!lista.getConsultation(i).getBeginDate().after(nowy.getEnd()) && !lista.getConsultation(i).getEndDate().before(nowy.getBegin())) { 
                    throw new PropertyVetoException("Listener PropertyVetoException", null); 
                }
            }
        }
    }
}
